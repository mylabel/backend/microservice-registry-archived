<?php

namespace Database\Factories;

use App\Models\Service;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'address' => $this->faker->ipv4()
        ];
    }

    public function withOnlineStatus(): ServiceFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => 'Online',
            ];
        });
    }

    public function withOfflineStatus(): ServiceFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => 'Offline',
            ];
        });
    }
}
