<?php

namespace Feature;

use App\Models\Service;
use Illuminate\Support\Facades\Http;
use Laravel\Lumen\Testing\DatabaseMigrations;

class ServiceRegistryControllerTest extends \TestCase
{
    use DatabaseMigrations;

    public function testRegisterSuccess()
    {
        $service = Service::factory()->make();

        $this->post('/service/register', [
            'name' => $service->name,
            'address' => $service->address
        ]);

        $this->seeInDatabase('services', $service->toArray());
        $this->assertResponseOk();
    }

    public function testRegisterEmptyName()
    {
        $service = Service::factory()->make();

        $this->post('/service/register', [
            'address' => $service->address
        ]);

        $this->assertResponseStatus(422);
    }

    public function testRegisterEmptyAddress()
    {
        $service = Service::factory()->make();

        $this->post('/service/register', [
            'name' => $service->name
        ]);

        $this->assertResponseStatus(422);
    }

    public function testHeartbeatOnlineSuccess()
    {
        Http::fake();

        $service = Service::factory()->withOnlineStatus()->create();

        $this->put('/service/heartbeat', [
            'name' => $service->name,
            'address' => $service->address
        ]);

        $this->seeInDatabase('services', $service->toArray());
        $this->assertResponseOk();
    }

    public function testHeartbeatOfflineSuccess()
    {
        Http::fake();

        $service = Service::factory()->withOfflineStatus()->create();

        $this->put('/service/heartbeat', [
            'name' => $service->name,
            'address' => $service->address
        ]);

        $this->assertResponseOk();
    }

    public function testHeartbeatSuccess()
    {
        Http::fake();

        $service = Service::factory()->make();

        $this->put('/service/heartbeat', [
            'name' => $service->name,
            'address' => $service->address
        ]);

        $this->seeInDatabase('services', $service->toArray());
        $this->assertResponseOk();
    }

    public function testHeartbeatEmptyName()
    {
        $service = Service::factory()->make();

        $this->put('/service/heartbeat', [
            'address' => $service->address
        ]);

        $this->assertResponseStatus(422);
    }

    public function testHeartbeatEmptyAddress()
    {
        $service = Service::factory()->make();

        $this->put('/service/heartbeat', [
            'name' => $service->name
        ]);

        $this->assertResponseStatus(422);
    }

    public function testUnregisterSuccess()
    {
        Http::fake();

        $service = Service::factory()->withOnlineStatus()->create();

        $this->delete('/service/unregister', [
            'name' => $service->name,
            'address' => $service->address
        ]);

        $this->assertResponseOk();
    }

    public function testUnregisterEmptyName()
    {
        $service = Service::factory()->make();

        $this->delete('/service/unregister', [
            'address' => $service->address
        ]);

        $this->assertResponseStatus(422);
    }

    public function testUnregisterEmptyAddress()
    {
        $service = Service::factory()->make();

        $this->delete('/service/unregister', [
            'name' => $service->name
        ]);

        $this->assertResponseStatus(422);
    }

    public function testItemSuccess()
    {
        $service = Service::factory()->withOnlineStatus()->create();

        $this->get('/service/' . $service->name);

        $this->assertResponseOk();
    }

    public function testItemWrongName()
    {
        $service = Service::factory()->make();

        $this->get('/service/' . $service->name);

        $this->assertResponseStatus(404);
    }
}
