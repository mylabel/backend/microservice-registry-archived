<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    const accessLevels = [
        'create' => [
            0 => ['*']
        ],
        'get' => [
            0 => ['*']
        ],
        'update' => [
            0 => ['*']
        ],
        'delete' => [
            0 => ['*']
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'address', 'status'
    ];
}
