<?php

namespace App\Console;

use App\Models\Service;
use phpcommon\Utils\ServiceLog;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            collect(Service::where('updated_at', '<=', Carbon::now()->subMinutes(2)->toDateTimeString())->where(['status' => 'Online'])->get())->each(function ($service) {
                $service->status = 'Offline';
                $service->save();
                ServiceLog::error($service->name, 'Status changed from up to down. IP: ' . $service->address, Service::where(['name' => 'Notifications-Microservice'])->first()->address);
            });
            Http::post(Service::where(['name' => 'Notifications-Microservice'])->first()->address . '/system/monitoring', ['services' => Service::all()]);
        })->everyMinute();
    }
}
