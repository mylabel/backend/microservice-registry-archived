<?php

namespace App\Http\Controllers;

use App\Services\ServiceService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use phpcommon\http\Messages;
use phpcommon\http\ResponseMessagesDTO as ResponseDTO;
use phpcommon\http\ResponseProvider;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Throwable;


class ServiceRegistryController extends Controller
{
    private ServiceService $serviceService;
    private array $serviceRules = ['name' => 'required', 'address' => 'required'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ServiceService $serviceService)
    {
        $this->serviceService = $serviceService;
    }

    /**
     * @throws Throwable
     */
    public function register(Request $request): JsonResponse
    {
        $validatedData = $this->validate($request, $this->serviceRules);

        $this->serviceService->create($validatedData, -1);

        return ResponseProvider::render(new ResponseDTO(new Messages\SUCCESSFUL_REQUEST_Message));
    }

    /**
     * @throws Throwable
     */
    public function heartbeat(Request $request): JsonResponse
    {
        $validatedData = $this->validate($request, $this->serviceRules);
        if (!$validatedData['name'] = $this->serviceService->convertContainers($validatedData['name'])) {
            return ResponseProvider::render(new ResponseDTO(new Messages\SUCCESSFUL_REQUEST_Message));
        }

        if ($service = $this->serviceService->find(['name' => $validatedData['name']], -1)) {
            if ($service->status === 'Offline')
                $this->update();
            $this->serviceService->update('name', $validatedData['name'], $validatedData, -1);
        } else {
            $this->serviceService->create(array_merge($validatedData, ['address' => $validatedData['address']]), -1);
            $this->update();
        }

        return ResponseProvider::render(new ResponseDTO(new Messages\SUCCESSFUL_REQUEST_Message));
    }

    private function update()
    {
        $this->serviceService->hookEveryone();
    }

    /**
     * @throws Throwable
     */
    public function registrator(Request $request): JsonResponse
    {
        $validatedData = $this->validate($request, [
            'services' => 'required|array',
            'services.*.name' => 'string',
            'services.*.address' => 'string'
        ]);

        foreach ($validatedData['services'] as $serviceC) {
            if (!$serviceC['name'] = $this->serviceService->convertContainers($serviceC['name']))
                continue;

            if ($service = $this->serviceService->find(['name' => $serviceC['name']], -1)) {
                if ($service->status === 'Offline')
                    $this->update();
                $this->serviceService->update('name', $serviceC['name'], $serviceC, -1);
            } else {
                $this->serviceService->create(array_merge($serviceC, ['address' => $serviceC['address']]), -1);
                $this->update();
            }
        }

        return ResponseProvider::render(new ResponseDTO(new Messages\SUCCESSFUL_REQUEST_Message));
    }

    /**
     * @throws Throwable
     */
    public function unregister(Request $request): JsonResponse
    {
        $validatedData = $this->validate($request, $this->serviceRules);

        $this->serviceService->destroy($validatedData, -1);

        return ResponseProvider::render(new ResponseDTO(new Messages\SUCCESSFUL_REQUEST_Message));
    }

    /**
     * @throws Throwable
     */
    public function item(string $name): JsonResponse
    {
        throw_unless($service = $this->serviceService->find(['name' => $name], -1), new ModelNotFoundException);

        return ResponseProvider::render(new ResponseDTO(new Messages\SUCCESSFUL_REQUEST_Message, $service));
    }
}
