<?php

namespace App\Services;

use App\Models\Service;
use Illuminate\Support\Facades\Http;
use phpcommon\http\BaseService;
use phpcommon\Utils\MicroServices;

class ServiceService extends BaseService
{
    public function __construct()
    {
        parent::__construct(Service::class);
    }

    public function create(array $input, int $accessLevel)
    {
        $input['status'] = 'Online';
        return parent::create($input, $accessLevel);
    }

    public function update($column, $value, array $input, int $accessLevel)
    {
        $input['status'] = 'Online';
        return parent::update($column, $value, $input, $accessLevel);
    }

    public function hookEveryone()
    {
//        collect($this->all(-1))->each(function ($service) {
//            Http::post($service->address . '/hook', [Service::all(['name', 'address'])]);
//        });
    }

    public function convertContainers(string $containerName): ?string
    {
        return match (true) {
            str_contains($containerName, 'nginx_auth') => Microservices::AUTH_MICROSERVICE,
            str_contains($containerName, 'nginx_subscription') => MicroServices::SUBSCRIPTIONS_MICROSERVICE,
            str_contains($containerName, 'nginx_payment') => MicroServices::PAYMENT_MICROSERVICE,
            str_contains($containerName, 'nginx_ticket') => MicroServices::TICKETS_MICROSERVICE,
            str_contains($containerName, 'nginx_booking') => MicroServices::BOOKING_MICROSERVICE,
            str_contains($containerName, 'nginx_randomcover') => MicroServices::RANDOMCOVER_MICROSERVICE,
            str_contains($containerName, 'nginx_files') => MicroServices::FILES_MICROSERVICE,
            str_contains($containerName, 'nginx_notifi') => MicroServices::NOTIFICATIONS_MICROSERVICE,
            str_contains($containerName, 'nginx_registry') => MicroServices::MICROSERVICE_REGISTRY,
            str_contains($containerName, 'nginx_mylabel') => MicroServices::MYLABEL_MICROSERVICE,
            str_contains($containerName, 'nginx_muslink') => MicroServices::MUSLINK_MICROSERVICE,
            default => null,
        };
    }
}
