<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

$router->get('/', function () {
    return ['api' => env('APP_NAME')];
});

$router->post('/service/register', 'ServiceRegistryController@register');
$router->put('/service/heartbeat', 'ServiceRegistryController@heartbeat');
$router->put('/service/registrator', 'ServiceRegistryController@registrator');
$router->delete('/service/unregister', 'ServiceRegistryController@unregister');
$router->get('/service/{name}', 'ServiceRegistryController@item');
